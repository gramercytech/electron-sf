const { app, BrowserWindow } = require('electron');
const remote = require('electron').remote
// global.sharedObject = {prop1: process.argv}
const mode = process.argv[2]
const reader = process.argv[3]
const session = process.argv[4]

// npm start MODE READER SESSION
// ex: npm start 1 3 1

function createWindow () {
  win = new BrowserWindow({ 
    //width: 800, 
    //height: 600, 
    frame: false,
    // minimizable: false,
    alwaysOnTop: true,
    fullscreen: true
  })

  console.log('Mode:', mode)
  console.log('Reader', reader)
  console.log('Session', session)
  
  let url
  if (mode == '1') {
    url = 'http://10.0.0.104:8080/?1='+mode+'&reader='+reader+'&session='+session
  } else {
    url = 'http://10.0.0.104:8080/?reader='+reader+'&session='+session
  }
  console.log(url)
  win.loadURL(url)
}

app.on('ready', createWindow)
